# jinja2_pai_custom_filters

Jinja2 pai custom filters


## Description

Filters for cookiecutter packages templates


## Note

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
